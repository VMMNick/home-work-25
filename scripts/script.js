document.addEventListener("DOMContentLoaded", function () {
  const display = document.querySelector(".display input");
  const buttons = document.querySelectorAll(".button");
  let memory = null;
  let operator = null;
  let currentInput = "";

  buttons.forEach((button) => {
    button.addEventListener("click", function () {
      const value = button.value;
      if (!isNaN(value) || value === ".") {
        currentInput += value;
        display.value = currentInput;
      } else if (["+", "-", "*", "/"].includes(value)) {
        if (currentInput !== "") {
          memory = parseFloat(currentInput);
          operator = value;
          currentInput = "";
        }
      } else if (["m+", "m-", "mrc"].includes(value)) {
        if (value === "m+") {
          memory = memory
            ? memory + parseFloat(currentInput)
            : parseFloat(currentInput);
        } else if (value === "m-") {
          memory = memory
            ? memory - parseFloat(currentInput)
            : -parseFloat(currentInput);
        } else if (value === "mrc") {
          currentInput = memory ? memory.toString() : "";
          display.value = currentInput;
        }
      } else if (value === "C") {
        currentInput = "";
        display.value = "";
      } else if (value === "=") {
        if (operator && currentInput !== "") {
          const secondNumber = parseFloat(currentInput);
          switch (operator) {
            case "+":
              currentInput = (memory + secondNumber).toString();
              break;
            case "-":
              currentInput = (memory - secondNumber).toString();
              break;
            case "*":
              currentInput = (memory * secondNumber).toString();
              break;
            case "/":
              currentInput = (memory / secondNumber).toString();
              break;
          }
          display.value = currentInput;
          operator = null;
          memory = null;
        }
      }
    });
  });
  window.addEventListener("keydown", function (event) {
    const key = event.key;
    const allowedKeys = [
      "0",
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      ".",
      "+",
      "-",
      "*",
      "/",
      "Enter",
    ];
    if (allowedKeys.includes(key)) {
      const button = document.querySelector(`.button[value="${key}"]`);
      if (button) {
        button.click();
      }
    }
  });
});
